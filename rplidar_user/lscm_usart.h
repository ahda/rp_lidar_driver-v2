/**
  ******************************************************************************
  * @file    lscm_usart.h
  * @author  Andrew Yeung
  * @version 1.0
  * @date    
  * @brief   
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LSCM_USART_H
#define __LSCM_USART_H

#include "stm32f4xx_hal.h"
#include "stm32f4xx.h"

#define USART_BUF_SIZE  100

typedef struct
{
    uint8_t rx_data;
    uint8_t rx_index;
    uint8_t rx_buffer[USART_BUF_SIZE];
    uint8_t rx_complete;
}T_USART_Buffer;

int usart_reset_buffer (int usart_id);

extern T_USART_Buffer usart1;
extern T_USART_Buffer usart2;
extern T_USART_Buffer usart6;

void lscm_usart_init (void);

void usart1_printf (const char* format,...);
void usart1_printf_dma (const char* format,...);

void usart2_printf (const char* format,...);
void usart2_printf_dma (const char* format,...);

void usart6_printf (const char* format,...);
void usart6_printf_dma (const char* format,...);


int usart_enqueue_to_buffer (uint8_t usart_id, uint8_t data);

int usart1_reset_buffer (void);
int usart2_reset_buffer (void);
int usart6_reset_buffer (void);

#endif /* __LSCM_USART_H */
