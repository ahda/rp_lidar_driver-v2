#ifndef __RPLIDAR_DRIVER_H
#define __RPLIDAR_DRIVER_H
#include "main.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_ll_dma.h"
#include "stm32f4xx_ll_usart.h"
#include "cmsis_os.h"
#include <string.h>
#define LIDAR_LOOP_NUM 4
#define LIDAR_DEGREE 361
typedef struct
{
	uint16_t obstacle[5][361];
	uint16_t window_filter_obstacle[361];
	uint8_t numOfCollection;

}Lidar_Type;

extern Lidar_Type RPlidar;

void collect_lidar_Rawdata(void);
void StartUp_Reception_lidar(void);
uint8_t StartUp_ExpressScan_lidar(void);
uint8_t Stop_Scan_Lidar(void);
void huart_x_Test(void);
void processMessage(uint8_t* rxBuffer, uint16_t counterofBuffer);	
void HAL_UART_RxIdleCallback(UART_HandleTypeDef* huart);
void decompress_memory_data(void);
void new_run_setUp(void);
uint16_t Get_start_angle_q6(uint8_t angle_high_bit,uint8_t angle_low_bit);
uint16_t get_angleDiff(uint16_t current_angle,uint16_t pre_angle);
void Init_RPlidarStructure(void);
void lidar_circularBuffer_check(void);
int16_t Get_Distance(uint16_t angle);
int8_t unsignDegreeToSignDegree(uint8_t degree);
#endif

