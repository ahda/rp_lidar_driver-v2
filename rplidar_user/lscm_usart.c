/**
  ******************************************************************************
  * @file    lscm_usart.c
  * @author  Andrew Yeung
  * @version 1.0
  * @date    
  * @brief   
  ******************************************************************************
  */

#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "main.h"
#include "stm32f4xx_hal.h"
#include "lscm_usart.h"
#include "cmsis_os.h"

extern UART_HandleTypeDef huart1;
//extern UART_HandleTypeDef huart6;
extern UART_HandleTypeDef huart2;

extern osSemaphoreId binarySemUsart1RxHandle;
extern osSemaphoreId binarySemUsart2RxHandle;
//extern osSemaphoreId binarySemUsart6RxHandle;


T_USART_Buffer usart1;
T_USART_Buffer usart2;
T_USART_Buffer usart6;

/****************************************************************************/
//static void usart1_process_command (void);
//static void usart2_process_command (void);
//static void usart6_process_command (void);


//static char *zstring_strtok(char *str, const char *delim);
//static SpeedProfileParam_TypeDef get_pwm_control_parameter (void);
//static SpeedProfileParam_TypeDef get_pwm_control_parameter_2 (void);
/****************************************************************************/


#define USART_DMA_TX_BUF_SIZE   64

uint8_t txbuf1[USART_DMA_TX_BUF_SIZE];
uint8_t txbuf2[USART_DMA_TX_BUF_SIZE];
uint8_t txbuf6[USART_DMA_TX_BUF_SIZE];

void usart1_printf (const char* format,...)
{
    uint32_t length;
    va_list args;
    va_start(args, format);
    length = vsnprintf((char *)txbuf1, sizeof(txbuf1), (char *)format, args);
    va_end(args);

    for (int i=0; i<length; i++)
    {
        HAL_UART_Transmit(&huart1, (uint8_t *)&txbuf1[i], 1, 10);
    }
}

void usart1_printf_dma (const char* format,...)
{
    uint32_t length;
    va_list args;
    va_start(args, format);
    length = vsnprintf((char *)txbuf1, sizeof(txbuf1), (char *)format, args);
    va_end(args);

    HAL_UART_Transmit_DMA(&huart1, (uint8_t *)txbuf1, length);
}


void usart2_printf (const char* format,...)
{
    uint32_t length;
    va_list args;
    va_start(args, format);
    length = vsnprintf((char *)txbuf2, sizeof(txbuf2), (char *)format, args);
    va_end(args);

    for (int i=0; i<length; i++)
    {
        HAL_UART_Transmit(&huart2, (uint8_t *)&txbuf2[i], 1, 10);
    }
}

void usart2_printf_dma (const char* format,...)
{
    uint32_t length;
    va_list args;
    va_start(args, format);
    length = vsnprintf((char *)txbuf2, sizeof(txbuf2), (char *)format, args);
    va_end(args);

    HAL_UART_Transmit_DMA(&huart2, (uint8_t *)txbuf2, length);
}

//void usart6_printf(const char* format,...)
//{
//    uint32_t length;
//    va_list args;
//    va_start(args, format);
//    length = vsnprintf((char *)txbuf6, sizeof(txbuf6), (char *)format, args);
//    va_end(args);

//    for (int i=0; i<length; i++)
//    {
//        HAL_UART_Transmit(&huart6, (uint8_t *)&txbuf6[i], 1, 10);
//    }
//}

//void usart6_printf_dma (const char* format,...)
//{
//    uint32_t length;
//    va_list args;
//    va_start(args, format);
//    length = vsnprintf((char *)txbuf6, sizeof(txbuf6), (char *)format, args);
//    va_end(args);

//    HAL_UART_Transmit_DMA(&huart6, (uint8_t *)txbuf6, length);
//}



int usart_enqueue_to_buffer (uint8_t usart_id, uint8_t data)
{
    switch (usart_id)
    {
        case 1:
        {
            usart1.rx_buffer[usart1.rx_index++] = data;
            if (usart1.rx_index > USART_BUF_SIZE)
            {
                usart1.rx_index = 0;
            }
        }
        return 1;
            
        case 2:
        {
            usart2.rx_buffer[usart2.rx_index++] = data;
            if (usart2.rx_index > USART_BUF_SIZE)
            {
                //printf ("%s\r\n", usart2.rx_buffer);
                usart2.rx_index = 0;
            }
        }
        return 1;
        
        case 6:
        {
            usart6.rx_buffer[usart6.rx_index++] = usart6.rx_data;
            if (usart6.rx_index > USART_BUF_SIZE)
            {
               // printf ("%s\r\n", usart6.rx_buffer);
                usart6.rx_index = 0;
            }
        }
        return 1;
        
        default:
            return -1;
    }
}


int usart_reset_buffer (int usart_id)
{
    switch (usart_id)
    {
        case 1:
        {
            return usart1_reset_buffer();
        }
            
        case 2:
        {
            return usart2_reset_buffer();
        }
        
        case 6:
        {
            return usart6_reset_buffer();
        }
        
        default:
            return -1;
    }
}

int usart1_reset_buffer (void)
{
    for (int i=0; i<USART_BUF_SIZE; i++)
    {
        usart1.rx_buffer[i] = 0x00;
    }
    usart1.rx_index = 0;
    
    if (usart1.rx_buffer[0] == 0x00)
    {
        return USART_BUF_SIZE;
    }
    else
    {
        return -1;
    }
}

int usart2_reset_buffer (void)
{
    for (int i=0; i<USART_BUF_SIZE; i++)
    {
        usart2.rx_buffer[i] = 0x00;
    }
    usart2.rx_index = 0;
    
    if (usart2.rx_buffer[0] == 0x00)
    {
        return USART_BUF_SIZE;
    }
    else
    {
        return -1;
    }
}    

int usart6_reset_buffer (void)
{
    for (int i=0; i<USART_BUF_SIZE; i++)
    {
        usart6.rx_buffer[i] = 0x00;
    }
    usart6.rx_index = 0;
    
    if (usart6.rx_buffer[0] == 0x00)
    {
        return USART_BUF_SIZE;
    }
    else
    {
        return -1;
    }
}




void lscm_usart_init (void)
{
    usart1.rx_complete = 0;
    usart2.rx_complete = 0;
    usart6.rx_complete = 0;
    
    usart1_reset_buffer();
    usart2_reset_buffer();
    usart6_reset_buffer();

}
